<?php
echo "<h4>Point</h4>";
class Point{
	public $x1;
	public $y1;
		public function setX1($x1){
			$this->x1 = $x1;
		}
		public function setY1($y1){
			$this->y1 = $y1;
		}
		public function getX1(){
			return $this->x1;
		}
		public function getY1(){
			return $this->y1;
		}
		public function print_p(){
		 echo " x1=". $this->x1 . "<br> y1=" . $this->y1;
	 }
}
$point = new Point;
$point->setX1(11);
$point->setY1(12);

$point->print_p();
$getX1 = $point->getX1();
$getY1 = $point->getY1();
echo "<br>---get---<br>x1=". $getX1 . "<br> y1=" . $getY1;

//-----------------------------Line---------------------------------------
echo "<h4>Line</h4>";
class Line extends Point{
	public $x2;
	public $y2;
	public $x3;
	public $y3;
		public function setX2($x2){
			$this->x2 = $x2;
		}
		public function setY2($y2){
			$this->y2 = $y2;
		}
		public function setX3($x3){
			$this->x3 = $x3;
		}
		public function setY3($y3){
			$this->y3 = $y3;
		}
		//-------------------------isOnLine----------------------------
		public function isOnLine(){
	 		if(($this->x3 - $this->x1)/($this->x2 - $this->x1) == ($this->y3- $this->y1)/($this->y2 - $this->y1)){
	 			echo "<br>Is on Line";
	 		} else{
	 			echo "<br>Not is on Line";
	 		}
	 	}
		public function print_l(){
		 echo " x1=". $this->x1 . "<br> y1=" . $this->y1 . "<br> x2=". $this->x2 . "<br> y2=" . $this->y2;
	 }
}
$line = new Line();
$line->setX1(1);
$line->setY1(2);
$line->setX2(2);
$line->setY2(3);
$line->setX3(4);
$line->setY3(1);
$line->print_l();
$line->isOnLine();

//-----------------------------Triangle---------------------------------------
echo "<h4>Triangle</h4>";
class Triangle extends line{
			public function print_t(){
			echo " x1=". $this->x1 . "<br> y1=" . $this->y1 . "<br> x2=". $this->x2 . "<br> y2=" . $this->y2 . "<br> x3=". $this->x3 . "<br> y3=" . $this->y3;
	 }
}
$triangle = new Triangle;
$triangle->setX1(11);
$triangle->setY1(22);
$triangle->setX2(43);
$triangle->setY2(34);
$triangle->setX3(55);
$triangle->setY3(66);
$triangle->print_t();

//-----------------------------Quadranglee---------------------------------------
echo "<h4>Quadrangle</h4>";
class Quadrangle extends Triangle{
	public $x4;
	public $y4;
		public function setX4($x4){
			$this->x4 = $x4;
		}
		public function setY4($y4){
			$this->y4 = $y4;
		}
		public function print_r(){
			echo " x1=". $this->x1 . "<br> y1=" . $this->y1 . "<br> x2=". $this->x2 . "<br> y2=" . $this->y2 . "<br> x3=". $this->x3 . "<br> y3=" . $this->y3 . "<br> x4=". $this->x4 . "<br> y4=" . $this->y4;

		}

}
$quadrangle = new Quadrangle();

$quadrangle->setX1(11);
$quadrangle->setY1(22);
$quadrangle->setX2(43);
$quadrangle->setY2(34);
$quadrangle->setX3(55);
$quadrangle->setY3(66);
$quadrangle->setX4(8);
$quadrangle->setY4(18);
$quadrangle->print_r();